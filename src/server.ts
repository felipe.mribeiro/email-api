import express from 'express'

const app = express () //inicia a função do express

// http://localhost:3001/
app.get('/', (req, res) => {
    return res.json({message: 'hello api!!!'})
})

app.post('/', (req, res) => {
    return res.json({message: 'post data'})
})

app.listen(3001, () => console.log('server rodando'))